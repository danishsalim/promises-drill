const fs = require("node:fs");

function makeDir() {
  let promise = new Promise((resolve, reject) => {
    fs.mkdir("jason", { recursive: true }, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve("directory created");
      }
    });
  });
  return promise;
}

function createFile() {
  let promises = [];
  for (let index = 0; index < 5; index++) {
    let promise = new Promise((resolve, reject) => {
      fs.writeFile(`jason/file${index}`, `this is file${index}`, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve(`file${index} is created`);
        }
      });
    });
    promises.push(promise);
  }
  return Promise.all(promises);
}

function deleteFile(filename) {
  let promises = [];
  for (let index = 0; index < 5; index++) {
    let promise = new Promise((resolve, reject) => {
      fs.unlink(`jason/file${index}`,(error) => {
        if (error) {
          reject(error);
        } else {
          resolve(`file${index} is deleted`);
        }
      });
    });
    promises.push(promise);
  }
  return Promise.all(promises);
}



  module.exports={makeDir,createFile,deleteFile}