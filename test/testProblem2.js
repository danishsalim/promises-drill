const { readFile, writeFile, appendFile, deleteFile } = require("../Problem2");

readFile("lipsum_2.txt")
  .then((data) => {
    let uppercaseData = data.toUpperCase();
    return writeFile("uppercase.txt", uppercaseData);
  })
  .then((filename) => {
    console.log(filename + " created");
    return writeFile("filenames.txt", filename);
  })
  .then((filename) => {
    console.log(filename + " created");
    return readFile("uppercase.txt");
  })
  .then((uppercaseData) => {
    let lowercaseData = uppercaseData.toLowerCase();
    let sentences = lowercaseData.split(".").join("\n");
    return writeFile("sentences.txt", sentences);
  })
  .then((filename) => {
    console.log(filename, "created");
    return appendFile("filenames.txt", filename);
  })
  .then((message) => {
    console.log(message);
    return readFile("sentences.txt");
  })
  .then((data) => {
    let sortedData = data.split("\n").sort().join("\n");
    return writeFile("sortedData.txt", sortedData);
  })
  .then((filename) => {
    console.log(filename, " created");
    return appendFile("filenames.txt", filename);
  })
  .then((message) => {
    console.log(message);
    return readFile("filenames.txt");
  })
  .then((data) => {
    const files = data.split("\n");
    let promises = files.map((file) => {
      return deleteFile(file);
    });
    return Promise.all(promises);
  })
  .then((message) => {
    console.log(message);
  })
  .catch((error) => {
    console.log(error);
  });
