const {makeDir,createFile,deleteFile} =require('../Problem1')
const fs = require('node:fs')

makeDir()
  .then((message) => {
    console.log(message);
    return createFile();
  })
  .then((message) => {
    console.log(message);
    return deleteFile()
  })
  .then((data)=>{
    console.log(data)
  })
  .catch((error) => {
    console.log("there is error", error.message);
  });