const fs = require("node:fs");

function readFile(filename) {
  let promise = new Promise((resolve, reject) => {
    fs.readFile(filename, "utf-8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
  return promise;
}

function writeFile(filename, data) {
  let promise = new Promise((resolve, reject) => {
    fs.writeFile(filename, data, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve(filename);
      }
    });
  });
  return promise;
}

function appendFile(filename, data) {
  let promise = new Promise((resolve, reject) => {
    fs.appendFile(filename, "\n" + data, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve("new data added to " + filename);
      }
    });
  });
  return promise;
}

function deleteFile(filename) {
  let promise = new Promise((resolve, reject) => {
    fs.unlink(filename, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve(filename + " is deleted");
      }
    });
  });
  return promise;
}

module.exports = { readFile, writeFile, appendFile, deleteFile };
